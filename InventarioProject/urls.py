"""InventarioProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from InventarioApp import views
from django.urls import path
from InventarioApp.views import Producto_api_view
from InventarioApp.views import Producto_detalle_api_view

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    #path('producto/',views.ProductoView.as_view()),
    path('producto/', Producto_api_view,name='producto_api'),
    path('producto/<int:pk>/', Producto_detalle_api_view,name='producto_detalle_api'),
    #path('producto/<int:pk>/', views.ProductoDetailView.as_view())
]
