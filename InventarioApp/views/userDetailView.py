from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from InventarioApp.models.user import User
from InventarioApp.serializers.userSerializer import UserSerializer

class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    
    def get(self, request, *args, **kwargs):
        
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        return super().get(request, *args, **kwargs)

    def delete (request, id, rol):
        user = User.objects.get(pk=id)
        user.delete()  

        #if rol != 'True':
            #stringResponse = {'detail':'Unauthorized Request'}
        #return Response("No tiene los permisos suficientes para esta acción")
    
    def put(self, request, *args, **kwargs):
        
        id = request.data["id"]
        user = User.objects.get(pk=id)
        user.set_username = request.data["username"]
        user.set_password = request.data["password"]
        user.set_email = request.data["email"]
        user.set_Tipodoc = request.data["Tipodoc"]
        user.set_IdDoc = request.data["IdDoc"]

        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        stringResponse = {'detail':'User Updated'}

        return Response(stringResponse.validated_data, status=status.HTTP_200_OK)