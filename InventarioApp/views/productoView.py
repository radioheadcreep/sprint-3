from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from InventarioApp.serializers.productoSerializer import ProductoSerializer
from rest_framework.decorators import api_view
from InventarioApp.models import Producto

@api_view(['GET', 'POST'])
def Producto_api_view(request):
    if request.method == 'GET':
        entidades = Producto.objects.all()
        entidades_serializer = ProductoSerializer(entidades, many=True)

        #user = request.user
        #if user.role == 'administracion':
            #return Response(entidades_serializer.data)
            
        return Response(entidades_serializer.data)
        
    elif request.method == 'POST':
        entidades_serializer = ProductoSerializer(data=request.data)
        if entidades_serializer.is_valid():
            entidades_serializer.save()
            return Response(entidades_serializer.data)
        return Response(entidades_serializer.error_messages)


@api_view(['GET', 'PUT', 'DELETE'])
def Producto_detalle_api_view(request, pk=None):
    if request.method == 'GET':
        entidades = Producto.objects.filter(codigo = pk).first()
        entidades_serializer = ProductoSerializer(entidades)
        return Response(entidades_serializer.data)
    elif request.method == 'PUT':
        entidades = Producto.objects.filter(codigo = pk).first()
        entidades_serializer = ProductoSerializer(
            entidades, data=request.data)
        if entidades_serializer.is_valid():
            entidades_serializer.save()
            return Response(entidades_serializer.data)
        return Response(entidades_serializer.error_messages)
    elif request.method == 'DELETE':
        entidades = Producto.objects.filter(codigo = pk).first()
        entidades.delete()
        return Response('Eliminación Exitosa')

