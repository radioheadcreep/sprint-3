from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework import views
from InventarioApp.models.producto import Producto
from InventarioApp.serializers.productoSerializer import ProductoSerializer

class ProductoDetailView(views.APIView):
    @csrf_exempt
    def Producto_detail(request, pk):
        """
        Recupera, actualiza o elimina un Producto.
        """
        try:
            producto = Producto.objects.get(pk=pk)
        except Producto.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = ProductoSerializer(producto)
            return JsonResponse(serializer.data)

        elif request.method == 'PUT':
            data = JSONParser().parse(request)
            serializer = ProductoSerializer(producto, data=data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data)
            return JsonResponse(serializer.errors, status=400)

        elif request.method == 'DELETE':
            producto.delete()
            return HttpResponse(status=204)