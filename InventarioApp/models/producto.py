from django.db import models
from .user import User


class Producto(models.Model):
    codigo = models.IntegerField('producto',primary_key=True)
    cantidad = models.IntegerField('cantidad')
    nameproducto = models.CharField('nameproducto', max_length = 20,unique=True)
    precio= models.IntegerField('precio')
    user = models.ForeignKey(User, related_name='usuario', on_delete=models.CASCADE)
    
    