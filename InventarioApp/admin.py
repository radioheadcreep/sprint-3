from django.contrib import admin



from .models.user import User
#from .models.account import Account
from .models.producto import Producto

# Register your models here.
admin.site.register(User)
admin.site.register(Producto)
#admin.site.register(Account)